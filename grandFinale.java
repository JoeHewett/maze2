import uk.ac.warwick.dcs.maze.logic.IRobot;
import java.util.Random;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

public class grandFinale {
    
    private RobotData robotData; 
    int pollRun;
    boolean followMode;
    int resetCounter; 

    public void controlRobot(IRobot robot) {
        
        int direction = IRobot.AHEAD;
        
        if ((robot.getRuns() == 0) && (pollRun == 0))
            robotData = new RobotData(); 
            
        System.out.println("GET RUNS IS " + robot.getRuns() + " - POLL RUN IS " + pollRun); 

        if (resetCounter > 0 && robot.getRuns() == 0) {
            robotData = new RobotData();
            resetCounter = 0;
        }
        
        if (robot.getRuns() == 0) {
            followMode = false;
        } else {
            followMode = true;
        }

        pollRun++;
        
        direction = exploreControl(robot, robotData, followMode); 
            
        robot.face(direction); 
    }

    private int exploreControl(IRobot robot, RobotData robotData, boolean followMode) {
        int direction = 0;
        int exits = nonWallExits(robot);

        switch(exits) {
            case 1:
                direction = robotDeadEnd(robot);
                break;
            case 2: 
                direction = robotCorridor(robot);
                break;
            default:
                direction = robotJunction(robot, robotData, followMode);
                break;
        }
        return direction;
     }

    private int getOppositeHeading(IRobot robot, int heading) {
        int oppositeHeading = IRobot.NORTH + ((heading + 2) % 4);
        return oppositeHeading; 
    }

    private int translateHeading(IRobot robot, int heading) {
        int currentHeading = robot.getHeading();
        int desiredHeading = heading;
        int headingDifference = desiredHeading - currentHeading;
        int direction;

        if (headingDifference < 0)
            headingDifference += 4;

        direction = IRobot.AHEAD + headingDifference;

        return direction; 
    }

    // DEADEND LOGIC - Scan all directions and pick first direction that is not a wall
    private int robotDeadEnd(IRobot robot) {
        int direction = 0;

        for (int i = 0; i < 4; i++) {
            if (robot.look(IRobot.AHEAD + i) != IRobot.WALL)
                direction = IRobot.AHEAD + i;
        }
        return direction;
    }
   
    // CORRIDOR LOGIC - Only 1 direction (besides back) will be available
    // uses IF statements to check AHEAD, LEFT, RIGHT.
    private int robotCorridor(IRobot robot) {
        int direction = 0;

        if (robot.look(IRobot.AHEAD) != IRobot.WALL)
            direction = IRobot.AHEAD; 
        else if (robot.look(IRobot.LEFT) != IRobot.WALL) 
            direction = IRobot.LEFT;
        else 
            direction = IRobot.RIGHT;
        return direction;
    
    }

    // JUNCTION LOGIC - use passageExists to determine if one of the branches is unexplored
    // if unexplored, called getRandomDir with TRUE to ensure passage path is picked
    // if all explored, call getRandomDir with FALSE to pick any unwalled direction 
    private int robotJunction(IRobot robot, RobotData robotData, boolean followMode) {
        int direction = IRobot.AHEAD;
        int heading = 0; 
        int bbExits = beenbeforeExits(robot);
        int surroundingMarks = 0; 
        int arrivalMarks = 0; 

        Junction currentJunction; 

        // If the junction is new, create a new junction object 
        // (which automatically +1 mark arrival direction) 
        // Then get this new junction object (with getLastJunction())
        // And call getLeastMarkedRoute() to get an unexplored route
        if (robotData.junctionExists(robot.getLocation().x, robot.getLocation().y) == false) {

            robotData.newJunction(robot, robot.getLocation().x, robot.getLocation().y, robot.getHeading());
            currentJunction = robotData.getLastJunction(); 

            System.out.println("I'm at a NEW junction - total marks = " + currentJunction.getTotalMarks());
            heading = currentJunction.getLeastMarkedHeading(robot);

            System.out.println("Least marked heading is " + heading);

            direction = translateHeading(robot, heading); 

        } else {
            
            currentJunction = robotData.searchJunction(robot.getLocation().x, robot.getLocation().y, robot);
    
            if (followMode) {
                System.out.println("I'm in follow mode at a junction");
                System.out.println("I'm travelling in heading " + robot.getHeading());
                heading = currentJunction.getExitHeading(robot, getOppositeHeading(robot, robot.getHeading()));
                System.out.println("My chosen heading is " + heading);
                direction = translateHeading(robot, heading);
                
                return direction; 
            }       

        
            surroundingMarks = currentJunction.getTotalMarks();
            
            System.out.println("I'm at an OLD junction - total marks = " + currentJunction.getTotalMarks());

            currentJunction.addMark(getOppositeHeading(robot,robot.getHeading()));

            arrivalMarks = currentJunction.getMarks(getOppositeHeading(robot, robot.getHeading()));
            //System.out.println("Arrival marks = " + arrivalMarks); 
            if(surroundingMarks > 1 && arrivalMarks == 1) {
                System.out.println("we need to go back!");
                direction = IRobot.BEHIND;
                heading = getOppositeHeading(robot, robot.getHeading());
            } else if(arrivalMarks == 2) {
                System.out.println("Arrival has 2 marks so choose lowest marked exit");
                heading = currentJunction.getLeastMarkedHeading(robot);
                direction = translateHeading(robot, heading);
            } else {
                System.out.println("A big big fucky wucky has occured@@@@@@@@@@@@@@@@@@@@@@@@@");
            }
        }

        currentJunction.addMark(heading);
        currentJunction.drawJunction();
        return direction;
    }


    // RANDOM DIRECTION - choose a random direction from all 4 available directions - check first that no WALL exists
    // If passage = true then ensure that the chosen direction is also a passage
    private int getRandomDirection(IRobot robot, boolean passage) {
        int[] allDirections = {IRobot.AHEAD, IRobot.LEFT, IRobot.RIGHT, IRobot.BEHIND};
        int direction = IRobot.AHEAD;
        Random r = new Random();
        if (passage) {
            do {
                direction = allDirections[r.nextInt(4)];
            } while(robot.look(direction) == IRobot.WALL || robot.look(direction) != IRobot.PASSAGE);
        }
        else {
            do {
                direction = allDirections[r.nextInt(4)];
            } while(robot.look(direction) == IRobot.WALL);        
        }
        return direction;
 
    }

    
    // PASSAGE EXISTS - check to see if a passage exists around the robot
    private boolean passageExists(IRobot robot) {
        boolean exists = false;
        
        for (int i=0; i<4; i++) {
            if (robot.look(IRobot.AHEAD + i) == IRobot.PASSAGE) { 
                exists = true;
            }
                   
        }
        return exists; 
    }

    // Check to see how many nonWall exits exist by rotating to each of the 4 directions and checking for walls
    private int nonWallExits (IRobot robot) {
        int numNonWallExits = 0;

        for (int i = 0; i < 4; i++) {
            if (robot.look(IRobot.AHEAD + i) != IRobot.WALL)
                numNonWallExits += 1;
        }   

        return numNonWallExits;
    }

    private int beenbeforeExits (IRobot robot) {
        int beenbeforeExits = 0;

        for (int i = 0; i < 4; i++) {
            if (robot.look(IRobot.AHEAD + i) == IRobot.BEENBEFORE)
                beenbeforeExits += 1;
        }   

        return beenbeforeExits;      
    }

    public void reset() {
        robotData.resetJunctionCounter();
        resetCounter += 1;
    }


}

class Junction {
    public Point robotPoint;
    public int heading;
    // marks for N=0, E=1, S=2, W=3
    public int[] marks = {0, 0, 0, 0};
    IRobot robot;

    Junction() {
        
    }
 
    Junction(IRobot robo, int xcord, int ycord, int arrivalHeading) {
        int oppositeHeading;
        int index; 
        robotPoint = new Point(xcord, ycord);
        heading = arrivalHeading;
        robot = robo;
        oppositeHeading = getOppositeHeading(robot, arrivalHeading);
        addMark(oppositeHeading);  
        
    }
    
    private int getOppositeHeading(IRobot robot, int heading) {
        int oppositeHeading = 0;
        oppositeHeading = IRobot.NORTH + ((heading + 2) % 4);
        return oppositeHeading; 
    }

    public void addMark(int heading) {
        System.out.println("ADDING MARK FOR HEADING " + heading);
        int index = headingToIndex(robot, heading); 
        marks[index] += 1;  
    }

    public int getMarks(int heading) {
        int numMarks = 0;
        int index;
        index = headingToIndex(robot, heading); 
        numMarks = marks[index];
        return numMarks;
    }

    public int getTotalMarks() {
        int totalMarks = 0;

        for(int i = 0; i < 4; i++) {
            totalMarks += marks[i];
        }
        return totalMarks;
    }

    private int lookHeading(IRobot robot, int heading) {
        int currentHeading = robot.getHeading();
        int desiredHeading = heading;
        int headingDifference = desiredHeading - currentHeading;
        
        if(headingDifference < 0)
            headingDifference += 4;
    
        return robot.look(IRobot.AHEAD + headingDifference); 
        
    }

    public int getExitHeading(IRobot robot, int heading) {
        int exitHeading = 0;
        for(int i = 0; i < 4; i++) {
            if((marks[i] == 1) && (indexToHeading(robot, i) != heading)) {
                exitHeading = indexToHeading(robot, i);
            }
        }
        return exitHeading;      
    }

  

    public int getLeastMarkedHeading(IRobot robot) {
        int heading = -1; 
        int newBestHeading = -1;
        int leastMarks = 100;
        Random r = new Random();
        int lowestMarkedExits[] = {-1,-1,-1,-1};
        int randomIndex = -1; 

        for(int i = 0; i < 4; i++) {
            if(marks[i] < leastMarks) {
                heading = indexToHeading(robot, i);
                if(lookHeading(robot, heading) != IRobot.WALL) 
                    leastMarks = marks[i];
            }
        }

        System.out.println("The least marked exit has " + leastMarks + " marks");

        for(int i = 0; i < 4; i++) {
            if((marks[i] == leastMarks) && (lookHeading(robot, indexToHeading(robot, i)) != IRobot.WALL))
                lowestMarkedExits[i] = i;
        }

        System.out.println(lowestMarkedExits[0] + "   " + lowestMarkedExits[1] + "  " + lowestMarkedExits[2] + "  " + lowestMarkedExits[3]);

        do {
            randomIndex = r.nextInt(4);
        } while(lowestMarkedExits[randomIndex] == -1);
  
        newBestHeading = indexToHeading(robot, lowestMarkedExits[randomIndex]);

        return newBestHeading;
    }

    public int headingToIndex(IRobot robot, int heading) {
        int index = -1;

        switch(heading) {
            case IRobot.NORTH: 
                index = 0;
                break;
            case IRobot.EAST: 
                index = 1;
                break;
            case IRobot.SOUTH: 
                index = 2;
                break;
            case IRobot.WEST:     
                index = 3;
                break;
        }
        return index; 
    }
     public int indexToHeading(IRobot robot, int index) {
        int heading = 0;

        switch(index) {
            case 0: 
                heading = IRobot.NORTH;
                break;
            case 1: 
                heading = IRobot.EAST;
                break;
            case 2: 
                heading = IRobot.SOUTH;
                break;
            case 3:     
                heading = IRobot.WEST;
                break;
        }
        return heading; 
   } 
    
    public void drawJunction() {
        System.out.println("==== " + getMarks(IRobot.NORTH) + " ====");
        System.out.println("= " + getMarks(IRobot.WEST) + " =" + "o" + "= " + getMarks(IRobot.EAST) + " =");
        System.out.println("==== " + getMarks(IRobot.SOUTH) + " ====");

    }

    public int getHeading() {
        return heading;
    }

    public int getX() {
        int X = (int)robotPoint.getX();
        return X;
    }

    public int getY() {
        int Y = (int)robotPoint.getY();
        return Y;
    }
}


class RobotData {

    List<Junction> junctions;
    int junctionCounter;
    
    RobotData() {
        junctions = new ArrayList<>();
        junctionCounter = 0;
    }

    public Junction getLastJunction() {
        Junction lastJunction = junctions.get(junctions.size() - 1);
        return lastJunction; 
    }

    public boolean junctionExists(int x, int y) {
        boolean exists = false; 
        for(int i = 0; i < junctions.size(); i++) {
            if((junctions.get(i).getX() == x) && (junctions.get(i).getY() == y))
                exists = true; 
        }

        return exists; 
    }

   
    public Junction searchJunction(int currentX, int currentY, IRobot robot) {
        int junctionX = 0;
        int junctionY = 0; 
        Junction currentJunction = new Junction(); 

        for(int i=0; i < junctions.size(); i++) { 
            junctionX = junctions.get(i).getX();
            junctionY = junctions.get(i).getY();

            if((junctionX == currentX) && (junctionY == currentY)) {
                currentJunction = junctions.get(i);
            }
        }

        return currentJunction;
    }

    public void incrementJunctions() {
        junctionCounter += 1;
    }

    public void resetJunctionCounter() {
        junctionCounter = 0;
    }

    public void resetJunctionList() {
        junctions.clear();
    }

    public void newJunction(IRobot robot, int x, int y, int heading) {
        junctions.add(new Junction(robot, x, y, heading)); 
        incrementJunctions();
        
    }
}

