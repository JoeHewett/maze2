import uk.ac.warwick.dcs.maze.logic.IRobot;
import java.util.Random;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

public class ex1 {
    
    private RobotData robotData; 
    int pollRun = 0;
    private boolean explore;

    public void controlRobot(IRobot robot) {
        int direction = IRobot.AHEAD;
        int randno;
        int exits;

        // resets the robotData when it is the first run of a maze
        if ((robot.getRuns() == 0) && (pollRun == 0)) {
            robotData = new RobotData();
            explore = true;
        }

        pollRun++;
     

        // determine whether we are backtracking or exploring    
        if (explore == true) {
            direction = exploreControl(robot, robotData);
        } else 
            direction = backtrackControl(robot, robotData);
            
        robot.face(direction); 
    }

    // the default control method that determines the robots behaviour in each environment 
    // in the case the robot finds itself at a deadend, assuming the robot is not on its first move (pollRun > 0)
    // the robots explore mode is set to false and the robot continues the next move in backtrack mode
    private int exploreControl(IRobot robot, RobotData robotData) {
        int direction = 0;
        int exits = nonWallExits(robot);

        switch(exits) {
            case 1:
                direction = robotDeadEnd(robot);

                if(pollRun > 1) 
                    explore = false;
                break;
            case 2: 
                direction = robotCorridor(robot);
                break;
            default:
                direction = robotJunction(robot, robotData);

                break;
        }
        return direction;
     }

    // backtrack control acts in a similar manner to explore control with the addition of a junction check that determines 
    // whether or not the junction has been fully explored
    // in the case the junction has not been fully explored the robot is set back into explore mode
    // if the junction has been fully explored then the list of junctions is scanned to find the heading 
    // that the robot was facing when it first came into contact with the junction 
    // this heading is retrieved from the junction object and translated into a direction which the robot then follows
    private int backtrackControl(IRobot robot, RobotData robotData) {
        int direction = 0;
        int originalHeading = 0;
        int oppositeHeading = 0;
        int exits = nonWallExits(robot);

        switch(exits) {
            case 1:
                direction = robotDeadEnd(robot);
                break;
            case 2:
                direction = robotCorridor(robot);
                break;
            default:
                if(passageExists(robot)) {
                    explore = true; 
                    direction = robotJunction(robot, robotData);

                } else {
                    originalHeading = robotData.searchJunction(robot.getLocation().x, robot.getLocation().y, robot);
                    oppositeHeading = IRobot.NORTH + ((originalHeading + 2) % 4);
                    
                    direction = translateHeading(robot, oppositeHeading);
                }
                break;
        }
        return direction;
    }

    // function for translating between the provided heading and the represented direction
    private int translateHeading(IRobot robot, int heading) {
        int currentHeading = robot.getHeading();
        int desiredHeading = heading;
        int headingDifference = desiredHeading - currentHeading;
        int direction;

        if (headingDifference < 0)
            headingDifference += 4;

        direction = IRobot.AHEAD + headingDifference;

        return direction; 
    }

    // DEADEND LOGIC - Scan all directions and pick first direction that is not a wall
    private int robotDeadEnd(IRobot robot) {
        int direction = 0;

        for (int i = 0; i < 4; i++) {
            if (robot.look(IRobot.AHEAD + i) != IRobot.WALL)
                direction = IRobot.AHEAD + i;
        }
        return direction;
    }
   
    // CORRIDOR LOGIC - Only 1 direction (besides back) will be available
    // uses IF statements to check AHEAD, LEFT, RIGHT.
    private int robotCorridor(IRobot robot) {
        int direction = 0;

        if (robot.look(IRobot.AHEAD) != IRobot.WALL)
            direction = IRobot.AHEAD; 
        else if (robot.look(IRobot.LEFT) != IRobot.WALL) 
            direction = IRobot.LEFT;
        else 
            direction = IRobot.RIGHT;
        return direction;
    
    }

    // JUNCTION LOGIC - use passageExists to determine if one of the branches is unexplored
    // if unexplored, called getRandomDir with TRUE to ensure passage path is picked
    // if all explored, call getRandomDir with FALSE to pick any unwalled direction 
    private int robotJunction(IRobot robot, RobotData robotData) {
        int direction = IRobot.AHEAD;

        int bbExits = beenbeforeExits(robot);

        if (bbExits == 0 || bbExits == 1) {
            robotData.newJunction(robot.getLocation().x, robot.getLocation().y, robot.getHeading());
        }
        if (passageExists(robot))
            direction = getRandomDirection(robot, true);
        else 
            direction = getRandomDirection(robot, false); 

        return direction;
    }


    // RANDOM DIRECTION - choose a random direction from all 4 available directions - check first that no WALL exists
    // If passage = true then ensure that the chosen direction is also a passage
    private int getRandomDirection(IRobot robot, boolean passage) {
        int[] allDirections = {IRobot.AHEAD, IRobot.LEFT, IRobot.RIGHT, IRobot.BEHIND};
        int direction = IRobot.AHEAD;
        Random r = new Random();
        if (passage) {
            do {
                direction = allDirections[r.nextInt(4)];
            } while(robot.look(direction) == IRobot.WALL || robot.look(direction) != IRobot.PASSAGE);
        }
        else {
            do {
                direction = allDirections[r.nextInt(4)];
            } while(robot.look(direction) == IRobot.WALL);        
        }
        return direction;
 
    }

    
    // PASSAGE EXISTS - check to see if a passage exists around the robot
    private boolean passageExists(IRobot robot) {
        boolean exists = false;
        
        for (int i=0; i<4; i++) {
            if (robot.look(IRobot.AHEAD + i) == IRobot.PASSAGE) { 
                exists = true;
            }
                   
        }
        return exists; 
    }

    // Check to see how many nonWall exits exist by rotating to each of the 4 directions and checking for walls
    private int nonWallExits (IRobot robot) {
        int numNonWallExits = 0;

        for (int i = 0; i < 4; i++) {
            if (robot.look(IRobot.AHEAD + i) != IRobot.WALL)
                numNonWallExits += 1;
        }   

        return numNonWallExits;
    }

    // scan each direction and increment the exits for each exits direction
    private int beenbeforeExits (IRobot robot) {
        int beenbeforeExits = 0;

        for (int i = 0; i < 4; i++) {
            if (robot.look(IRobot.AHEAD + i) == IRobot.BEENBEFORE)
                beenbeforeExits += 1;
        }   

        return beenbeforeExits;      
    }

    // called on reset to reset the state of the maze so that the robot will not jib out on the next run
    // due to still having the list of junctions from the last run 
    public void reset() {
        robotData.resetJunctionCounter();
        robotData.resetJunctionList();
        explore = true;
    }


}

// Junction class that contains the information for each junction object
// robotPoint - a point object containing the x and y info for each junction
// heading - the heading which the robot was facing when it arrived at the junction 
class Junction {
    public Point robotPoint;
    public int heading;

    Junction(int xcord, int ycord, int arrivalHeading) {
        robotPoint = new Point(xcord, ycord);
        heading = arrivalHeading;
    }

    public void displayJunction() {
        System.out.println(robotPoint.getX() + ", " + robotPoint.getY() + " - Heading arrived from: " + heading);
    }

    // getter for heading
    public int getHeading() {
        return heading;
    }

    // getter for x - casts to int because point returns a double
    public int getX() {
        int X = (int)robotPoint.getX();
        return X;
    }

    // getting for y - casts to int because point returns a double
    public int getY() {
        int Y = (int)robotPoint.getY();
        return Y;
    }
}


class RobotData {

    // a list object that will store all of the junctions for this run of the maze
    List<Junction> junctions = new ArrayList<>();
    
    int junctionCounter;
    
    RobotData() {
        junctionCounter = 0;
    }

    // scans the list of junctions for the junction at the provided x and y co-ordinate and uses the heading getter 
    // to return the value of heading 
    public int searchJunction(int currentX, int currentY, IRobot robot) {
        int heading = 0;
        int oppositeHeading = 0; 
        int i;
        int junctionX = 0;
        int junctionY = 0; 

        for(i=0; i < junctionCounter; i++) { 
            junctionX = junctions.get(i).getX();
            junctionY = junctions.get(i).getY();

            if((junctionX == currentX) && (junctionY == currentY)) {
                heading = junctions.get(i).getHeading();
            }
        }

        return heading;
    }

    // prints the juncton info for the most recently added junctin
    public void printJunction() {
        junctions.get(junctionCounter).displayJunction();
    }

    public void incrementJunctions() {
        junctionCounter += 1;
    }

    public void resetJunctionCounter() {
        junctionCounter = 0;
    }

    public void resetJunctionList() {
        junctions.clear();
    }

    // add a junction to the list of junctions 
    public void newJunction(int x, int y, int heading) {
        junctions.add(new Junction(x, y, heading)); 
        incrementJunctions();
        
    }
}

