import uk.ac.warwick.dcs.maze.logic.IRobot;
import java.util.Random;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

public class ex3 {
    
    private RobotData robotData; 
    int pollRun = 0;
    int resetCounter = 0;

    public void controlRobot(IRobot robot) {
        int direction = IRobot.AHEAD;
        int randno;
        int exits;
        
        if (((robot.getRuns() == 0) && (pollRun == 0)) || resetCounter > 0) {
            resetCounter = 0;
            robotData = new RobotData();
        }

        pollRun++;
        
        direction = exploreControl(robot, robotData);
            
        robot.face(direction); 
    }

    private int exploreControl(IRobot robot, RobotData robotData) {
        int direction = 0;
        int exits = nonWallExits(robot);

        switch(exits) {
            case 1:
                direction = robotDeadEnd(robot);
                break;
            case 2: 
                direction = robotCorridor(robot);
                break;
            default:
                direction = robotJunction(robot, robotData);
                break;
        }
        return direction;
     }

    private int getOppositeHeading(IRobot robot, int heading) {
        int oppositeHeading = IRobot.NORTH + ((heading + 2) % 4);
        return oppositeHeading; 
    }

    private int translateHeading(IRobot robot, int heading) {
        int currentHeading = robot.getHeading();
        int desiredHeading = heading;
        int headingDifference = desiredHeading - currentHeading;
        int direction;

        if (headingDifference < 0)
            headingDifference += 4;

        direction = IRobot.AHEAD + headingDifference;

        return direction; 
    }

    // DEADEND LOGIC - Scan all directions and pick first direction that is not a wall
    private int robotDeadEnd(IRobot robot) {
        int direction = 0;

        for (int i = 0; i < 4; i++) {
            if (robot.look(IRobot.AHEAD + i) != IRobot.WALL)
                direction = IRobot.AHEAD + i;
        }
        return direction;
    }
   
    // CORRIDOR LOGIC - Only 1 direction (besides back) will be available
    // uses IF statements to check AHEAD, LEFT, RIGHT.
    private int robotCorridor(IRobot robot) {
        int direction = 0;

        if (robot.look(IRobot.AHEAD) != IRobot.WALL)
            direction = IRobot.AHEAD; 
        else if (robot.look(IRobot.LEFT) != IRobot.WALL) 
            direction = IRobot.LEFT;
        else 
            direction = IRobot.RIGHT;
        return direction;
    
    }

    // JUNCTION LOGIC - first determines if the current junction exits
    // if !exists then create a new junction, retrieve it, get a random low marked heading
    // if exists then search the list for the junction object
    //      if program in follow mode (runs >= 1) then find the exit with 1 mark and follow it
    //      else get the number of surrounding marks and use it to determine what to do based on Tremauxs Algorithm
    //      e.g. if arrival marks = 1 (first time from this direction) but junction marks > 1 (junction visited before from different direction) then turn around and go back where we came from
    //      else if arrival marks = 2 (we've been through this entrance twice) then choose a random lowest marked exit and follow it
    private int robotJunction(IRobot robot, RobotData robotData) {
        int direction = IRobot.AHEAD;
        int heading = 0; 
        int bbExits = beenbeforeExits(robot);
        int surroundingMarks = 0; 
        int arrivalMarks = 0; 

        Junction currentJunction; 


        // If the junction is new, create a new junction object 
        // (which automatically +1 mark arrival direction) 
        // Then get this new junction object (with getLastJunction())
        // And call getLeastMarkedRoute() to get an unexplored route
        if (robotData.junctionExists(robot.getLocation().x, robot.getLocation().y) == false) {

            robotData.newJunction(robot, robot.getLocation().x, robot.getLocation().y, robot.getHeading());
            currentJunction = robotData.getLastJunction(); 

            heading = currentJunction.getLeastMarkedHeading(robot);

            direction = translateHeading(robot, heading); 


        // if the junction is not new, then locate the junction in the list using x and y cords
        // determine if we are following old route or exploring. if follow, simply find the route with 1 mark and take it
        // if not following (exploring first time) then follow laws of Tremaux algorithm 
        // note: mark added on arrival direction here
        } else {
            
            currentJunction = robotData.searchJunction(robot.getLocation().x, robot.getLocation().y, robot);
            surroundingMarks = currentJunction.getTotalMarks();
            
            currentJunction.addMark(getOppositeHeading(robot,robot.getHeading()));

            arrivalMarks = currentJunction.getMarks(getOppositeHeading(robot, robot.getHeading()));
            if(surroundingMarks > 1 && arrivalMarks == 1) {
                direction = IRobot.BEHIND;
                heading = getOppositeHeading(robot, robot.getHeading());
            } else if(arrivalMarks == 2) {
                heading = currentJunction.getLeastMarkedHeading(robot);
                direction = translateHeading(robot, heading);
            } else {
                System.out.println("CATASTROPHIC FAILURE MISSION ABORT");
            }
        }

        currentJunction.addMark(heading);
        //currentJunction.drawJunction();
        return direction;
    }


    // PASSAGE EXISTS - check to see if a passage exists around the robot
    private boolean passageExists(IRobot robot) {
        boolean exists = false;
        
        for (int i=0; i<4; i++) {
            if (robot.look(IRobot.AHEAD + i) == IRobot.PASSAGE) 
                exists = true;
        }
        return exists; 
    }

    // Check to see how many nonWall exits exist by rotating to each of the 4 directions and checking for walls
    private int nonWallExits (IRobot robot) {
        int numNonWallExits = 0;

        for (int i = 0; i < 4; i++) {
            if (robot.look(IRobot.AHEAD + i) != IRobot.WALL)
                numNonWallExits += 1;
        }   

        return numNonWallExits;
    }


    // check to see how many beenbofore exits exist by rotating to each of the 4 directons and counting beenbefore squares
    private int beenbeforeExits (IRobot robot) {
        int beenbeforeExits = 0;

        for (int i = 0; i < 4; i++) {
            if (robot.look(IRobot.AHEAD + i) == IRobot.BEENBEFORE)
                beenbeforeExits += 1;
        }   

        return beenbeforeExits;      
    }

    public void reset() {
        resetCounter += 1;
    }


}


// junction objects that a created when a new junction is encountered and stored in a list of junctions
// inside robotData. Junction stores the co-ordinates of the junction and the direction of arrival 

class Junction {
    public Point robotPoint;
    public int heading;
    // marks for N=0, E=1, S=2, W=3
    // every junction object has an array of marks that stores the number of times each exit or entrance has been taken
    // this is incremented with the function add mark
    public int[] marks = {0, 0, 0, 0};
    IRobot robot;

    Junction() {
        
    }
 
    // constructor for a junction - sets the variables and adds a mark to the arrival direction 
    Junction(IRobot robo, int xcord, int ycord, int arrivalHeading) {
        int oppositeHeading;
        int index; 
        robotPoint = new Point(xcord, ycord);
        heading = arrivalHeading;
        robot = robo;
        oppositeHeading = getOppositeHeading(robot, arrivalHeading);
        addMark(oppositeHeading);  
        
    }
    
    // a useful function for converting a heading into its opposite heading using modulo 
    private int getOppositeHeading(IRobot robot, int heading) {
        int oppositeHeading = 0;
        oppositeHeading = IRobot.NORTH + ((heading + 2) % 4);
        return oppositeHeading; 
    }

    // a vital function that adds a mark to the specified heading
    // called once when the robot enters a junction (with the opposite of the arrival heading) then one
    // immediately before robot.face() is called to mark the chosen exit direction.
    // simply adds a mark to the array of marks for the current junction object using headingToIndex()
    public void addMark(int heading) {
        int index = headingToIndex(robot, heading); 
        marks[index] += 1;  
    }

    // a function to sum the marks at the given heading uses headingToIndex() to find the array index
    // that represents the given heading and returns the value found in that index
    public int getMarks(int heading) {
        int numMarks = 0;
        int index;
        index = headingToIndex(robot, heading); 
        numMarks = marks[index];
        return numMarks;
    }

    // tremaux uses logic based on the total marks at a junction. This function sums the numbers in the marks array and returns it
    public int getTotalMarks() {
        int totalMarks = 0;

        for(int i = 0; i < 4; i++) {
            totalMarks += marks[i];
        }
        return totalMarks;
    }

    // tremaux deals with headings and as such this function is required to allow robot to look in a heading and return what it sees
    // useful when we are determining which direction to go as must check for walls
    // takes the robot to use robot.look and a heading. Translates using modulus arithmetic
    private int lookHeading(IRobot robot, int heading) {
        int currentHeading = robot.getHeading();
        int desiredHeading = heading;
        int headingDifference = desiredHeading - currentHeading;
        
        if(headingDifference < 0)
            headingDifference += 4;
    
        return robot.look(IRobot.AHEAD + headingDifference); 
        
    } 

    // function for finding the least marked heading
    // scans through the marks and finds the exit with least marks
    // sets the heading to the index of that mark and then checks if that direction is a wall
    // if it is not a wall, set that heading to newBestHeading and set leastMarks to that index of marks
    public int getLeastMarkedHeading(IRobot robot) {
        int heading = -1; 
        int newBestHeading = -1;
        int leastMarks = 100;
        Random r = new Random();
        // if an exit is found with marks = lowest then we add its index to this array.
        // then we can check this array for where the value != -1 and we know that exit has lowest marks
        int lowestMarkedExits[] = {-1,-1,-1,-1};
        int randomIndex = -1; 

        // scan through all exits and check marks to find lowest number where heading != wall
        for(int i = 0; i < 4; i++) {
            if(marks[i] < leastMarks) {
                heading = indexToHeading(robot, i);
                if(lookHeading(robot, heading) != IRobot.WALL) 
                    leastMarks = marks[i];
            }
        }

        // using our lowestMarks number, add all exits to array where marks = lowestMarks
        for(int i = 0; i < 4; i++) {
            if((marks[i] == leastMarks) && (lookHeading(robot, indexToHeading(robot, i)) != IRobot.WALL))
                lowestMarkedExits[i] = i;
        }

        // randomly select an index and check the array of lowest marks to see if it exists. 
        // do until a selection is made where selected index != -1 
        do {
            randomIndex = r.nextInt(4);
        } while(lowestMarkedExits[randomIndex] == -1);
  
        newBestHeading = indexToHeading(robot, lowestMarkedExits[randomIndex]);

        return newBestHeading;
    }


    public int headingToIndex(IRobot robot, int heading) {
        int index = -1;

        switch(heading) {
            case IRobot.NORTH: 
                index = 0;
                break;
            case IRobot.EAST: 
                index = 1;
                break;
            case IRobot.SOUTH: 
                index = 2;
                break;
            case IRobot.WEST:     
                index = 3;
                break;
        }
        return index; 
    }
     public int indexToHeading(IRobot robot, int index) {
        int heading = 0;

        switch(index) {
            case 0: 
                heading = IRobot.NORTH;
                break;
            case 1: 
                heading = IRobot.EAST;
                break;
            case 2: 
                heading = IRobot.SOUTH;
                break;
            case 3:     
                heading = IRobot.WEST;
                break;
        }
        return heading; 
   } 
 
    // a function for debugging that prints a visual representation of the marks at a junction    
    public void drawJunction() {
        System.out.println("==== " + getMarks(IRobot.NORTH) + " ====");
        System.out.println("= " + getMarks(IRobot.WEST) + " =" + "o" + "= " + getMarks(IRobot.EAST) + " =");
        System.out.println("==== " + getMarks(IRobot.SOUTH) + " ====");

    }

    public int getHeading() {
        return heading;
    }

    public int getX() {
        int X = (int)robotPoint.getX();
        return X;
    }

    public int getY() {
        int Y = (int)robotPoint.getY();
        return Y;
    }
}


class RobotData {

    // the list that stores the junctions each run 
    List<Junction> junctions;
    
    // initalised in the constructor
    RobotData() {
        junctions = new ArrayList<>();
    }

    // returns the most recently created junction by using junction.size - 1 
    public Junction getLastJunction() {
        Junction lastJunction = junctions.get(junctions.size() - 1);
        return lastJunction; 
    }

    // function for checking if a junction exists in the list using x and y cords
    public boolean junctionExists(int x, int y) {
        boolean exists = false; 
        for(int i = 0; i < junctions.size(); i++) {
            if((junctions.get(i).getX() == x) && (junctions.get(i).getY() == y))
                exists = true; 
        }

        return exists; 
    }


    // returns the junction at the provided x and y
    public Junction searchJunction(int currentX, int currentY, IRobot robot) {
        int junctionX = 0;
        int junctionY = 0; 
        Junction currentJunction = new Junction(); 

        for(int i=0; i < junctions.size(); i++) { 
            junctionX = junctions.get(i).getX();
            junctionY = junctions.get(i).getY();

            if((junctionX == currentX) && (junctionY == currentY)) {
                currentJunction = junctions.get(i);
            }
        }

        return currentJunction;
    }

    // add a new junction object to the list junctions
    public void newJunction(IRobot robot, int x, int y, int heading) {
        junctions.add(new Junction(robot, x, y, heading)); 
    }
}

