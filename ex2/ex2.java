import uk.ac.warwick.dcs.maze.logic.IRobot;
import java.util.Stack;
import java.util.Random;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

public class ex2 {
    
    private RobotData robotData; 
    int pollRun = 0;
    private boolean explore;

    // main control method that determines the current explore state and decides whether
    // to enter backtrack or explore mode.
    public void controlRobot(IRobot robot) {
        int direction = IRobot.AHEAD;

        // determine if on first move. If first move then reset robotData to wipe info
        if ((robot.getRuns() == 0) && (pollRun == 0)) {
            robotData = new RobotData();
            explore = true;
        }

        pollRun++;
        
        if (explore == true) {
            direction = exploreControl(robot, robotData);
        } else 
            direction = backtrackControl(robot, robotData);
            
        robot.face(direction); 
    }

    // explore mode is the default state and follows standard movement behaviour - if 1 exit the deadend = turnaround
    // if 2 exits then corridor = continue on and 3+ exits = junction so follow junction logic
    // the method forces explore mode to 0 when a deadend is encountered so that the next run will start in backtrack mode
    private int exploreControl(IRobot robot, RobotData robotData) {
        int direction = 0;
        int exits = nonWallExits(robot);

        switch(exits) {
            case 1:
                direction = robotDeadEnd(robot);

                if(pollRun > 1) 
                    explore = false;
                break;
            case 2: 
                direction = robotCorridor(robot);
                break;
            default:
                direction = robotJunction(robot, robotData);

                break;
        }
        return direction;
     }

    // used when a deadend is encountered
    // behaves like explore with the exception of fully explored junctions where the robot pops the arrival direction from a stack
    // and follows it, to allow the robot to navigate back to unexplored territories 
    // if a junction with an unexplored exit is encountered then the robot forces explore to 1 and calls robotJunction
    private int backtrackControl(IRobot robot, RobotData robotData) {
        int direction = 0;
        int originalHeading = 0;
        int oppositeHeading = 0;
        int exits = nonWallExits(robot);

        switch(exits) {
            case 1:
                direction = robotDeadEnd(robot);
                break;
            case 2:
                direction = robotCorridor(robot);
                break;
            default:
                if(passageExists(robot)) {
                    explore = true; 
                    direction = robotJunction(robot, robotData);

                } else {
                    originalHeading = robotData.popJunction(); 
                    oppositeHeading = IRobot.NORTH + ((originalHeading + 2) % 4);
                    
                    direction = translateHeading(robot, oppositeHeading);
                }
                break;
        }
        return direction;
    }
    
    // translate a heading to a direction - for use with backtrackControl when popping an absolute direction from the stack
    // to translate this into a useable direction
    // utilises modulus arithmetic to translate between heading and direction
    private int translateHeading(IRobot robot, int heading) {
        int currentHeading = robot.getHeading();
        int desiredHeading = heading;
        int headingDifference = desiredHeading - currentHeading;
        int direction;

        if (headingDifference < 0)
            headingDifference += 4;

        direction = IRobot.AHEAD + headingDifference;

        return direction; 
    }

    // DEADEND LOGIC - Scan all directions and pick first direction that is not a wall
    private int robotDeadEnd(IRobot robot) {
        int direction = 0;

        for (int i = 0; i < 4; i++) {
            if (robot.look(IRobot.AHEAD + i) != IRobot.WALL)
                direction = IRobot.AHEAD + i;
        }
        return direction;
    }
   
    // CORRIDOR LOGIC - Only 1 direction (besides back) will be available
    // uses IF statements to check AHEAD, LEFT, RIGHT.
    private int robotCorridor(IRobot robot) {
        int direction = 0;

        if (robot.look(IRobot.AHEAD) != IRobot.WALL)
            direction = IRobot.AHEAD; 
        else if (robot.look(IRobot.LEFT) != IRobot.WALL) 
            direction = IRobot.LEFT;
        else 
            direction = IRobot.RIGHT;
        return direction;
    
    }

    // JUNCTION LOGIC - use passageExists to determine if one of the branches is unexplored
    // if unexplored, called getRandomDir with TRUE to ensure passage path is picked
    // if all explored, call getRandomDir with FALSE to pick any unwalled direction 
    private int robotJunction(IRobot robot, RobotData robotData) {
        int direction = IRobot.AHEAD;

        int bbExits = beenbeforeExits(robot);

        if (bbExits == 0 || bbExits == 1) {
            robotData.pushJunction(robot.getHeading());
        }
        if (passageExists(robot))
            direction = getRandomDirection(robot, true);
        else 
            direction = getRandomDirection(robot, false); 

        return direction;
    }


    // RANDOM DIRECTION - choose a random direction from all 4 available directions - check first that no WALL exists
    // If passage = true then ensure that the chosen direction is also a passage
    private int getRandomDirection(IRobot robot, boolean passage) {
        int[] allDirections = {IRobot.AHEAD, IRobot.LEFT, IRobot.RIGHT, IRobot.BEHIND};
        int direction = IRobot.AHEAD;
        Random r = new Random();
        if (passage) {
            do {
                direction = allDirections[r.nextInt(4)];
            } while(robot.look(direction) == IRobot.WALL || robot.look(direction) != IRobot.PASSAGE);
        }
        else {
            do {
                direction = allDirections[r.nextInt(4)];
            } while(robot.look(direction) == IRobot.WALL);        
        }
        return direction;
 
    }

    
    // PASSAGE EXISTS - check to see if a passage exists around the robot
    private boolean passageExists(IRobot robot) {
        boolean exists = false;
        
        for (int i=0; i<4; i++) {
            if (robot.look(IRobot.AHEAD + i) == IRobot.PASSAGE) { 
                exists = true;
            }
                   
        }
        return exists; 
    }

    // Check to see how many nonWall exits exist by rotating to each of the 4 directions and checking for walls
    private int nonWallExits (IRobot robot) {
        int numNonWallExits = 0;

        for (int i = 0; i < 4; i++) {
            if (robot.look(IRobot.AHEAD + i) != IRobot.WALL)
                numNonWallExits += 1;
        }   

        return numNonWallExits;
    }

    // perform a 360 spin and check if the faced direction is beenBefore. If it is, then increent the beenBeforeExits value
    private int beenbeforeExits (IRobot robot) {
        int beenbeforeExits = 0;

        for (int i = 0; i < 4; i++) {
            if (robot.look(IRobot.AHEAD + i) == IRobot.BEENBEFORE)
                beenbeforeExits += 1;
        }   

        return beenbeforeExits;      
    }


    // called when the reset button is pressed to set explore mode back to true for the next run
    public void reset() {
        explore = true;
    }


}


class RobotData {

    // stack of headings that is pushed to when encountering a new junction and popped from when ecountering an old
    // fully explored junction
    Stack<Integer> junctionStack = new Stack<Integer>();
    
    RobotData() {
    }

    // resets the stack for a new run to avoid the issue of the robot attempting to follow an old stack of values for a new maze 
    // for which the values will be incorrect and cause collisions
    public void resetJunctionStack() {
        junctionStack.clear(); 
    }

    // push a new heading onto the stack
    public void pushJunction(int heading) {
        junctionStack.push(heading);
        // code for pushing a new junction on  
    }

    // return the heading at the top of the stack 
    public int popJunction() {
        int direction = junctionStack.pop();
        return direction;
    }
}

