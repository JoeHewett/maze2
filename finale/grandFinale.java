import uk.ac.warwick.dcs.maze.logic.IRobot;
import java.util.Random;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

public class grandFinale {
    
    private RobotData robotData; 
    int pollRun;
    boolean followMode;
    int resetCounter; 

    // main control method - checks status of maze using .getRuns, pollRun and resetCounter
    // uses resetCounter to check if we have clicked newMaze so that followMode resets back to false
    // gets direction using single exploreControl method, with return depending on passed val of followMode
    public void controlRobot(IRobot robot) {
        
        int direction = IRobot.AHEAD;
        
        if ((robot.getRuns() == 0) && (pollRun == 0))
            robotData = new RobotData(); 

        // determines whether user has clicked the newMaze button 
        if (resetCounter > 0 && robot.getRuns() == 0) {
            robotData = new RobotData();
            resetCounter = 0;
        }
        
        if (robot.getRuns() == 0) {
            followMode = false;
        } else {
            followMode = true;
        }

        pollRun++;
        
        direction = exploreControl(robot, robotData, followMode); 
            
        robot.face(direction); 
    }

    // explore method that determines the direction. Simply identifies environment and call relevant
    // environment method (1 exit = deadEnd, 2 = corridor, 3 or 4 = junction)
    private int exploreControl(IRobot robot, RobotData robotData, boolean followMode) {
        int direction = 0;
        int exits = nonWallExits(robot);

        switch(exits) {
            case 1:
                direction = robotDeadEnd(robot);
                break;
            case 2: 
                direction = robotCorridor(robot);
                break;
            default:
                direction = robotJunction(robot, robotData, followMode);
                break;
        }
        return direction;
     }


    // tremaux requires manipulation of headings so this is a useful method for taking a heading
    // and returning the opposite heading e.g. NORTH returns SOUTH
    private int getOppositeHeading(IRobot robot, int heading) {
        int oppositeHeading = IRobot.NORTH + ((heading + 2) % 4);
        return oppositeHeading; 
    }

    // converts a heading to a direction by getting difference and modding by 4
    private int translateHeading(IRobot robot, int heading) {
        int currentHeading = robot.getHeading();
        int desiredHeading = heading;
        int headingDifference = desiredHeading - currentHeading;
        int direction;

        if (headingDifference < 0)
            headingDifference += 4;

        direction = IRobot.AHEAD + headingDifference;

        return direction; 
    }

    // DEADEND LOGIC - Scan all directions and pick first direction that is not a wall
    private int robotDeadEnd(IRobot robot) {
        int direction = 0;

        for (int i = 0; i < 4; i++) {
            if (robot.look(IRobot.AHEAD + i) != IRobot.WALL)
                direction = IRobot.AHEAD + i;
        }
        return direction;
    }
   
    // CORRIDOR LOGIC - Only 1 direction (besides back) will be available
    // uses IF statements to check AHEAD, LEFT, RIGHT.
    private int robotCorridor(IRobot robot) {
        int direction = 0;

        if (robot.look(IRobot.AHEAD) != IRobot.WALL)
            direction = IRobot.AHEAD; 
        else if (robot.look(IRobot.LEFT) != IRobot.WALL) 
            direction = IRobot.LEFT;
        else 
            direction = IRobot.RIGHT;
        return direction;
    
    }

    // JUNCTION LOGIC - first determines if the current junction exits
    // if !exists then create a new junction, retrieve it, get a random low marked heading
    // if exists then search the list for the junction object
    //      if program in follow mode (runs >= 1) then find the exit with 1 mark and follow it
    // else get the number of surrounding marks and use it to determine what to do based on Tremauxs Algorithm
    // e.g. if arrival marks = 1 (first time from this direction) but junction marks > 1 (junction visited before from different direction) then turn around and go back where we came from
    // else if arrival marks = 2 (we've been through this entrance twice) then choose a random lowest marked exit and follow it
    private int robotJunction(IRobot robot, RobotData robotData, boolean followMode) {
        int direction = IRobot.AHEAD;
        int heading = 0; 
        int surroundingMarks = 0; 
        int arrivalMarks = 0; 

        Junction currentJunction; 

        // if the junction is new, create a new junction object 
        // (which automatically +1 mark arrival direction) 
        // Then get this new junction object (with getLastJunction())
        // And call getLeastMarkedRoute() to get an unexplored route
        if (robotData.junctionExists(robot.getLocation().x, robot.getLocation().y) == false) {

            robotData.newJunction(robot, robot.getLocation().x, robot.getLocation().y, robot.getHeading());
            currentJunction = robotData.getLastJunction(); 

            heading = currentJunction.getLeastMarkedHeading(robot);

            direction = translateHeading(robot, heading); 


        // if the junction is not new, then locate the junction in the list using x and y cords
        // determine if we are following old route or exploring. if follow, simply find the route with 1 mark and take it
        // if not following (exploring first time) then follow laws of Tremaux algorithm 
        // note: mark added on arrival direction here
        } else {
            
            currentJunction = robotData.searchJunction(robot.getLocation().x, robot.getLocation().y, robot);
    
            if (followMode) {
                heading = currentJunction.getExitHeading(robot, getOppositeHeading(robot, robot.getHeading()));
                direction = translateHeading(robot, heading);
                return direction; 
            }       

            surroundingMarks = currentJunction.getTotalMarks();

            // add a mark to the direction from which we arrived
            currentJunction.addMark(getOppositeHeading(robot,robot.getHeading()));

            arrivalMarks = currentJunction.getMarks(getOppositeHeading(robot, robot.getHeading()));

            // tremaux algorithm 
            if(surroundingMarks > 1 && arrivalMarks == 1) {
                direction = IRobot.BEHIND;
                heading = getOppositeHeading(robot, robot.getHeading());
            } else if(arrivalMarks == 2) {
                heading = currentJunction.getLeastMarkedHeading(robot);
                direction = translateHeading(robot, heading);
            } else {
                System.out.println("CATASTROPHIC FAILURE MISSION ABORT");
            }
        }

        // before heading is returned, add a mark to show that we are exiting this direction
        currentJunction.addMark(heading);
        //currentJunction.drawJunction();
        return direction;
    }

        
    // check to see how many nonWall exits exist by rotating to each of the 4 directions and checking for walls
    private int nonWallExits (IRobot robot) {
        int numNonWallExits = 0;
        for (int i = 0; i < 4; i++) {
            if (robot.look(IRobot.AHEAD + i) != IRobot.WALL)
                numNonWallExits += 1;
        }   
        return numNonWallExits;
    }

    public void reset() {
        resetCounter += 1;
    }


}


// junction objects that a created when a new junction is encountered and stored in a list of junctions
// inside robotData. Junction stores the co-ordinates of the junction and the direction of arrival 
class Junction {
    public Point robotPoint;
    public int heading;

    // marks for N=0, E=1, S=2, W=3
    // every junction object has an array of marks that stores the number of times each exit or entrance has been taken
    // this is incremented with the function add mark
    public int[] marks = {0, 0, 0, 0};
    IRobot robot;

    Junction() {
        
    }

    // constructor for a junction - sets the variables and adds a mark to the arrival direction 
    Junction(IRobot robo, int xcord, int ycord, int arrivalHeading) {
        int oppositeHeading;
        int index; 
        robotPoint = new Point(xcord, ycord);
        heading = arrivalHeading;
        robot = robo;
        oppositeHeading = getOppositeHeading(robot, arrivalHeading);
        addMark(oppositeHeading);  
        
    }
    
    // a useful function for converting a heading into its opposite heading using modulo 
    private int getOppositeHeading(IRobot robot, int heading) {
        int oppositeHeading = 0;
        oppositeHeading = IRobot.NORTH + ((heading + 2) % 4);
        return oppositeHeading; 
    }

    // tremaux requires that all entrances and exits to and from junctions are marked when they are taken. This is 
    public void addMark(int heading) {
        int index = headingToIndex(robot, heading); 
        marks[index] += 1;  
    }
    
    // given a direction, check the marks on that tile 
    public int getMarks(int heading) {
        int numMarks = 0;
        int index;
        index = headingToIndex(robot, heading); 
        numMarks = marks[index];
        return numMarks;
    }

    public int getTotalMarks() {
        int totalMarks = 0;

        for(int i = 0; i < 4; i++) {
            totalMarks += marks[i];
        }
        return totalMarks;
    }

    private int lookHeading(IRobot robot, int heading) {
        int currentHeading = robot.getHeading();
        int desiredHeading = heading;
        int headingDifference = desiredHeading - currentHeading;
        
        if(headingDifference < 0)
            headingDifference += 4;
    
        return robot.look(IRobot.AHEAD + headingDifference); 
        
    }

    public int getExitHeading(IRobot robot, int heading) {
        int exitHeading = 0;
        for(int i = 0; i < 4; i++) {
            if((marks[i] == 1) && (indexToHeading(robot, i) != heading)) {
                exitHeading = indexToHeading(robot, i);
            }
        }
        return exitHeading;      
    }

    // when following tremauxs algorithm we sometimes need to find the lowest marked exits
    // and randomly choose between them. That is what this function does. 
    // it finds the least marked exit that is not a wall and set leastMarks to that value
    // it then loops through all of the possible exits 
    public int getLeastMarkedHeading(IRobot robot) {
        int heading = -1; 
        int newBestHeading = -1;
        int leastMarks = 100;
        Random r = new Random();
        // if an exit is found with marks = lowest then we add its index to this array.
        // then we can check this array for where the value != -1 and we know that exit has lowest marks
        int lowestMarkedExits[] = {-1,-1,-1,-1};
        int randomIndex = -1; 

        // scan through all exits and check marks to find lowest number where heading != wall
        for(int i = 0; i < 4; i++) {
            if(marks[i] < leastMarks) {
                heading = indexToHeading(robot, i);
                if(lookHeading(robot, heading) != IRobot.WALL) 
                    leastMarks = marks[i];
            }
        }

        // using our lowestMarks number, add all exits to array where marks = lowestMarks
        for(int i = 0; i < 4; i++) {
            if((marks[i] == leastMarks) && (lookHeading(robot, indexToHeading(robot, i)) != IRobot.WALL))
                lowestMarkedExits[i] = i;
        }

        // randomly select an index and check the array of lowest marks to see if it exists. 
        // do until a selection is made where selected index != -1 
        do {
            randomIndex = r.nextInt(4);
        } while(lowestMarkedExits[randomIndex] == -1);
  
        newBestHeading = indexToHeading(robot, lowestMarkedExits[randomIndex]);

        return newBestHeading;
    }

    // useful function for translating 0, 1, 2, 3 to IRobot.NORTH, EAST etc
    // useful because of the fact the marks arrays store the mark count in an index
    // that has a relationship with the headings e.g. index = NORTH 
    public int headingToIndex(IRobot robot, int heading) {
        int index = -1;

        switch(heading) {
            case IRobot.NORTH: 
                index = 0;
                break;
            case IRobot.EAST: 
                index = 1;
                break;
            case IRobot.SOUTH: 
                index = 2;
                break;
            case IRobot.WEST:     
                index = 3;
                break;
        }
        return index; 
    }
    public int indexToHeading(IRobot robot, int index) {
        int heading = 0;

        switch(index) {
            case 0: 
                heading = IRobot.NORTH;
                break;
            case 1: 
                heading = IRobot.EAST;
                break;
            case 2: 
                heading = IRobot.SOUTH;
                break;
            case 3:     
                heading = IRobot.WEST;
                break;
        }
        return heading; 
   } 
    
    public void drawJunction() {
        System.out.println("==== " + getMarks(IRobot.NORTH) + " ====");
        System.out.println("= " + getMarks(IRobot.WEST) + " =" + "o" + "= " + getMarks(IRobot.EAST) + " =");
        System.out.println("==== " + getMarks(IRobot.SOUTH) + " ====");

    }

    public int getHeading() {
        return heading;
    }

    public int getX() {
        int X = (int)robotPoint.getX();
        return X;
    }

    public int getY() {
        int Y = (int)robotPoint.getY();
        return Y;
    }
}


// stores the list of junctions and has some functions to manipulate the junction list
// e.g. getLastJunction to return the most recently created junction
// junctionExits to take coordinates and determine if a junction exits there
// searchJunction to take an x and y and return the junction that exists there
// newJunction to add a new junction object to the list of junctions
class RobotData {

    List<Junction> junctions;
    
    RobotData() {
        junctions = new ArrayList<>();
    }

    
    public Junction getLastJunction() {
        Junction lastJunction = junctions.get(junctions.size() - 1);
        return lastJunction; 
    }

    // simple check to ensure a junction exists at the provided x and y
    public boolean junctionExists(int x, int y) {
        boolean exists = false; 
        for(int i = 0; i < junctions.size(); i++) {
            if((junctions.get(i).getX() == x) && (junctions.get(i).getY() == y))
                exists = true; 
        }
        return exists; 
    }

    // finds the junction object at the given x and y and returns it
    public Junction searchJunction(int currentX, int currentY, IRobot robot) {
        int junctionX = 0;
        int junctionY = 0; 
        Junction currentJunction = new Junction(); 

        for(int i=0; i < junctions.size(); i++) { 
            junctionX = junctions.get(i).getX();
            junctionY = junctions.get(i).getY();

            if((junctionX == currentX) && (junctionY == currentY)) {
                currentJunction = junctions.get(i);
            }
        }

        return currentJunction;
    }

    public void newJunction(IRobot robot, int x, int y, int heading) {
        junctions.add(new Junction(robot, x, y, heading)); 
        
    }
}

